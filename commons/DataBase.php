<?php


class DataBase
{
    private $pdo;

    /**
     * DataBase constructor.
     */
    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=localhost;port=3301;dbname=test_client', 'root', '123admin456');
    }

    public function fetch($sqlQuery)
    {
        $statement = $this->pdo->prepare($sqlQuery);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_OBJ);
    }

    public function fetchOne($sqlQuery)
    {
        $statement = $this->pdo->prepare($sqlQuery);
        $statement->execute();

        return $statement->fetch(PDO::FETCH_OBJ);
    }

    public function executeQuery($sqlQuery)
    {
        $this->pdo->query($sqlQuery);
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @param PDO $pdo
     */
    public function setPdo($pdo)
    {
        $this->pdo = $pdo;
    }


}