<?php

require_once 'commons/DataBase.php';

class ClientDao extends DataBase
{

    public function fecthAll()
    {
        return $this->fetch("SELECT * FROM client");
    }

    public function findById($clientId)
    {
        return $this->fetchOne(
            "SELECT clt.*FROM client clt WHERE clt.id = ".$clientId
        );
    }

    public function persist($clientId, $nom, $prenom, $email)
    {
        if ($clientId) {
            $sqlQuery = "UPDATE `client`  SET `nom` = '".$nom."',`prenom` = '".$nom."',`email` = '".$email."' WHERE `id` = ".$clientId.";";
        } else {
            $sqlQuery = "INSERT INTO client (nom,prenom,email) VALUES ('".$nom."','".$prenom."','".$email."')";
        }

        $this->executeQuery($sqlQuery);
        $clientId = (is_null($clientId)) ? $this->getPdo()->lastInsertId() : $clientId;

        return $clientId;
    }

    public function persistVoitures($clientId, $voitures)
    {
        if (sizeof($voitures) > 0) {
            $sqlQueryDeleteVoiture = "DELETE FROM client_voiture WHERE client_id = ".$clientId;
            $this->executeQuery($sqlQueryDeleteVoiture);

            $sqlQueryInsertVoiture = "INSERT INTO client_voiture (client_id,voiture_id) VALUES ";
            foreach ($voitures as $voiture) {
                $sqlQueryInsertVoiture .= "(".$clientId.",".$voiture."),";
            }
            $sqlQueryInsertVoiture = substr($sqlQueryInsertVoiture, 0, strlen($sqlQueryInsertVoiture) - 1);
            $this->executeQuery($sqlQueryInsertVoiture);
        }
    }

    public function findByVoitureId($voitureId)
    {
        return $this->fetch(
            "SELECT clt.* FROM voiture vtr 
                LEFT JOIN client_voiture clt_vtr ON clt_vtr.voiture_id = vtr.id
                LEFT JOIN client clt ON clt.id = clt_vtr.client_id
                WHERE vtr.id = ".$voitureId
        );
    }

    public function findNameByVoitureId($voitureId)
    {
        return $this->fetch(
            "SELECT clt.name FROM voiture vtr 
                LEFT JOIN client_voiture clt_vtr ON clt_vtr.voiture_id = vtr.id
                LEFT JOIN client clt ON clt.id = clt_vtr.client_id
                WHERE vtr.id = ".$voitureId
        );
    }
}