<?php


class Voiture
{
    private $id;
    private $marque;
    private $typeMoteur;
    private $clients = array();

    /**
     * Voiture constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
    }

    /**
     * @return array
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @param array $clients
     */
    public function setClients($clients)
    {
        $this->clients = $clients;
    }

    /**
     * @return mixed
     */
    public function getTypeMoteur()
    {
        return $this->typeMoteur;
    }

    /**
     * @param mixed $typeMoteur
     */
    public function setTypeMoteur($typeMoteur)
    {
        $this->typeMoteur = $typeMoteur;
    }



}