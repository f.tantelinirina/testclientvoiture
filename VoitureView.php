<?php include 'header.php'; ?>
<?php
require './service/VoitureService.php';

$voitureService = new VoitureService();
?>
<div class="container">
    <p><a href="ClientEditView.php">Liste Voiture :</p>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Marque</th>
            <th>Type de Moteur</th>
            <th>Proriétaires</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($voitureService->retrieveAll() as $voiture) { ?>
            <tr>
                <td>
                    <?= $voiture->getId(); ?>
                </td>
                <td>
                    <?= $voiture->getMarque(); ?>
                </td>
                <td>
                    <?= $voiture->getTypeMoteur(); ?>
                </td>
                <td>
                    <?= $voitureService->joinClientName($voiture->getClients()) ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php include 'footer.php' ?>
