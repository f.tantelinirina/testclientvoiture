<?php
$clientId = isset($_GET['id']) ? $_GET['id'] : null;
require('./service/ClientService.php');
require('./service/VoitureService.php');
$clientService = new ClientService();
$voitureService = new VoitureService();

$nom = '';
$email = '';
$prenom = '';
$voitures = [];
$voitureClients = [];
$voitureAll = $voitureService->retrieveAll();
if ($clientId) {
    $client = $clientService->retrieveForId($clientId);
    if ($client) {
        $nom = $client->getNom();
        $prenom = $client->getPrenom();
        $email = $client->getEmail();
        $voitureClients = $client->getVoitures();
    }
} else {

}

if (isset($_POST['cdm_add_client'])) {
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $email = $_POST['email'];
    $voitures = $_POST['voitures'];

    if (($key = array_search('-1', $voitures)) !== false) {
        unset($voitures[$key]);
    }

    $clientId = $clientService->save($clientId, $nom, $prenom, $email);
    $clientService->saveVoiture($clientId, $voitures);

    header('Location: ClientView.php');
    exit();
}
?>
<?php include 'header.php'; ?>
<div class="container">
    <p>Edition Client :</p>
    <form action="#" method="POST">
        <div class="form-group">
            <label for="nom">Nom :</label>
            <input type="text" class="form-control" id="nom" name="nom" value="<?= $nom ?>">
        </div>
        <div class="form-group">
            <label for="prenom">Prenom :</label>
            <input type="text" class="form-control" id="prenom" name="prenom" value="<?= $prenom ?>">
        </div>
        <div class="form-group">
            <label for="pwd">Email : </label>
            <input type="email" class="form-control" id="email" name="email" value="<?= $email ?>"
                   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
        </div>
        <div class="form-group">
            <label for="sel1">Voitures :</label>
            <select class="form-control" multiple id="voitures[]" name="voitures[]">
                <option value="-1">Veuillez sélectionner.</option>
                <?php foreach ($voitureAll as $voiture) { ?>
                    <option value="<?= $voiture->getId() ?>"
                        <?= $voitureService->isSelectedVoiture(
                            $voiture,
                            $voitureClients
                        ) ?>>
                        <?= $voitureService->getVoitureNom($voiture) ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary" name="cdm_add_client">Valider</button>
    </form>
</div>
<?php include 'footer.php'; ?>
