<?php
require('./service/ClientService.php');
include 'header.php';

$clientService = new ClientService();
?>
<div class="container">
    <p><a href="ClientEditView.php">Ajouter Client</a> | Liste Client :</p>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($clientService->retrieveAll() as $client) { ?>
            <tr>
                <td>
                    <a href="ClientEditView.php?id=<?= $client->getId() ?>">
                        <?= $client->getId(); ?>
                    </a>
                </td>
                <td>
                    <a href="ClientEditView.php?id=<?= $client->getId() ?>">
                        <?= $client->getNom(); ?>
                    </a>
                </td>
                <td>
                    <a href="ClientEditView.php?id=<?= $client->getId() ?>">
                        <?= $client->getPrenom(); ?>
                    </a>
                </td>
                <td>
                    <a href="ClientEditView.php?id=<?= $client->getId() ?>">
                        <?= $client->getEmail(); ?>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php include 'footer.php'; ?>
