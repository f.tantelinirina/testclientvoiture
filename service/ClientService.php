<?php

//require('../dao/ClientDao.php');
//require('../model/Client.php');

require 'dao/ClientDao.php';
require 'model/Client.php';

class ClientService extends ClientDao
{

    public function retrieveAll()
    {
        $clientsData = [];
        $clients = $this->fecthAll();
        foreach ($clients as $client) {
            $clientData = new Client();
            $clientData->setId($client->id);
            $clientData->setNom($client->nom);
            $clientData->setPrenom($client->prenom);
            $clientData->setEmail($client->email);
            array_push($clientsData, $clientData);
        }

        return $clientsData;
    }

    public function retrieveForId($clientId)
    {
        $voitureService = new VoitureService();
        $client = $this->findById($clientId);
        $clientData = new Client();
        if ($client) {
            $clientData->setId($client->id);
            $clientData->setNom($client->nom);
            $clientData->setPrenom($client->prenom);
            $clientData->setEmail($client->email);
            $clientData->setVoitures($voitureService->retrieveForClientId($client->id));
        }

        return $clientData;
    }

    public function retrieveForVoitureId($voitureId)
    {
        $clients = $this->findByVoitureId($voitureId);
        $clientDatas = [];
        foreach ($clients as $client) {
            $clientData = new Client();
            $clientData->setId($client->id);
            $clientData->setNom($client->nom);
            $clientData->setPrenom($client->prenom);
            $clientData->setEmail($client->email);
            array_push($clientDatas, $clientData);
        }

        return $clientDatas;
    }

    public function save($clientId, $name, $prenom, $email)
    {
        return $this->persist($clientId, $name, $prenom, $email);
    }

    public function saveVoiture($clientId, $voitures)
    {
        return $this->persistVoitures($clientId, $voitures);
    }
}