<?php

require_once 'dao/VoitureDao.php';
require_once 'dao/VoitureDao.php';
require_once 'model/Voiture.php';
require_once 'service/ClientService.php';

class VoitureService extends VoitureDao
{

    public function retrieveAll()
    {
        $voituresData = [];
        $voitures = $this->fecthAll();
        $clientService = new ClientService();

        foreach ($voitures as $voiture) {
            $voitureData = new Voiture();
            $voitureData->setId($voiture->id);
            $voitureData->setMarque($voiture->marque);
            $voitureData->setTypeMoteur($voiture->type_moteur);
            $voitureData->setClients($clientService->retrieveForVoitureId($voiture->id));
            array_push($voituresData, $voitureData);
        }

        return $voituresData;
    }

    public function joinClientName($clients = array())
    {
        $clientNames = [];
        foreach ($clients as $client) {
            array_push($clientNames, $client->getNom());
        }

        return join($clientNames, ', ');
    }

    public function getVoitureNom($voiture)
    {
        return $voiture->getMarque().', '.$voiture->getTypeMoteur();
    }

    public function retrieveForClientId($clientId)
    {
        $voitures = $this->findByClientId($clientId);
        $voitreDatas = [];
        foreach ($voitures as $voiture) {
            $voitreData = new Voiture();
            $voitreData->setId($voiture->id);
            $voitreData->setMarque($voiture->marque);
            $voitreData->setTypeMoteur($voiture->type_moteur);
            array_push($voitreDatas, $voitreData);
        }

        return $voitreDatas;
    }

    public function isSelectedVoiture($voiture, $voitureClients)
    {
        foreach ($voitureClients as $voitureClient) {
            if ($voitureClient->getId() == $voiture->getId()) {
                return 'selected';
            }
        }

        return '';
    }
}