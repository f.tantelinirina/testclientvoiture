--
-- Current Database: `test_client`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `test_client` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test_client`;

-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: localhost    Database: test_client
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'CORENTIN','client1@gmail.com','CORENTIN'),(2,'DARA','client2@gmail.com','DARA'),(3,'BONNIE','client3@gmail.com','Thomas '),(4,'EDWIGE','client4@gmail.com','Petit '),(9,'FRANCESCA','client5@gmail.com','Robert '),(10,'GENOWEFA','client6@gmail.com','Richard '),(11,'HAINA','name7@gmail.com','Durand '),(12,'MATHIAS','name8@gmail.com','Dubois '),(13,'NATACHA','name9@gmail.com','NATACHA'),(14,'PARFAIT','john.doe@gmail.com','John');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_voiture`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_voiture` (
  `client_id` int(11) NOT NULL,
  `voiture_id` int(11) NOT NULL,
  PRIMARY KEY (`client_id`,`voiture_id`),
  KEY `client_voiture_fk2` (`voiture_id`),
  CONSTRAINT `client_voiture_fk1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `client_voiture_fk2` FOREIGN KEY (`voiture_id`) REFERENCES `voiture` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_voiture`
--

LOCK TABLES `client_voiture` WRITE;
/*!40000 ALTER TABLE `client_voiture` DISABLE KEYS */;
INSERT INTO `client_voiture` VALUES (4,1),(9,1),(14,1),(1,2),(2,2),(3,2),(4,2),(10,2),(11,2),(13,2),(2,3),(9,3),(14,3),(13,4),(13,5);
/*!40000 ALTER TABLE `client_voiture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voiture`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voiture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marque` varchar(255) NOT NULL,
  `type_moteur` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voiture`
--

LOCK TABLES `voiture` WRITE;
/*!40000 ALTER TABLE `voiture` DISABLE KEYS */;
INSERT INTO `voiture` VALUES (1,'Opel','1.6 diesel (1.6D)'),(2,'Opel','1.9 essence (1.9 TDI)'),(3,'Volkswagen','1.6 diesel (1.6D)'),(4,'Volkswagen','1.9 diesel (1.9D)'),(5,'Volkswagen','1.9 essence (1.9 TDI)');
/*!40000 ALTER TABLE `voiture` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-01 21:04:52
